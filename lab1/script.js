class LowArray {
    constructor(initArray = []) {
        this.array = [...initArray];
    }

    insert(value) {
        this.array.push(value);
    }

    find(value) {
        for (let item of this.array) {
            if (item === value) {
                return true;
            }
        }
        return false;

        // for (let i = 0; i < this.array.length; i++) {
        //     if (this.array[i] === value) {
        //         return i;
        //     }
        // }
        // return -1;
    }

    delete(value) {
        for (let i = 0; i < this.array.length; i++) {
            if (this.array[i] === value) {
                this.array.splice(i, 1);
                return true;
            }
        }
        return false;
    }
}

const la1 = new LowArray([1, 2, 3, 4, 5]); // [1, 2, 3, 4, 5]
const la2 = new LowArray();

for (let i = 0; i < 10; i++) {
    la2.insert(i);
    // [0]
    // [0, 1]
    // [0, 1, 2]
    console.log('insert', i, [...la2.array]);
}
// [0, 1, 2, ... 10]

console.log('find', 5, la2.find(5));   // true
console.log('find', 1000, la2.find(1000)); // false
console.log('delete', la2.delete(3));  // true [0, 1, 2, 4, 5, ..., 99]
console.log(la2);
console.log('delete', la2.delete(3));  // true [0, 1, 2, 4, 5, ..., 99]
console.log(la2);