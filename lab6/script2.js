function quickSort(arr, getPivotIndexFunc, left = 0, right = arr.length - 1) {
    if (left < right) {
        let p = partition(arr, getPivotIndexFunc, left, right); // p = pivot index

        quickSort(arr, getPivotIndexFunc, left, p - 1);
        quickSort(arr, getPivotIndexFunc, p + 1, right);
    }
}

function swap(arr, i, j) {
    if (i !== j) {
        let tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}

function getPivotIndexM(arr, left, right) {

    // выбор опорной точки (pivot) по медиане трех
    let mid = Math.floor((left + right) / 2);

    if (arr[left] > arr[mid]) {
        if (arr[right] > arr[left]) return left;
        return (arr[mid] > arr[right]) ? mid : right;
    }
    if (arr[right] > arr[mid]) return mid;
    return (arr[left] > arr[right]) ? left : right;
}

function getPivotIndexH(arr, left, right) {

    // выбор в качестве опорной точки (pivot) последнего элемента
    return right;
}

function getPivotIndexR(arr, left, right) {

    // выбор опорной точки (pivot) рандомно
    return Math.floor(Math.random() * (right - left + 1)) + left;
}

function partition(arr, getPivotFunc, left, right) {

    let pivotIndex = getPivotFunc(arr, left, right);
    swap(arr, right, pivotIndex);

    let pivot = arr[right];
    let i = left - 1;

    for (let j = left; j <= right - 1; j++) {
        if (arr[j] < pivot) {
            i++;
            swap(arr, i, j);
        }
    }

    swap(arr, i + 1, right);

    // console.log(arr, 'left:', left, 'right:', right, `p[${(i+1)}]=`, arr[i+1]);

    return i + 1;
}

/*
 * Test utils ================================================================================================
 */
function generateArr(n, min, max) {
    let arr = [];
    for (let i = 0; i < n; i++) {
        arr[i] = Math.floor(Math.random() * (max - min + 1)) + min;
    }
    return arr;
}
function check(arr) {
    for (let i = 0; i < arr.length - 1; i++) {
        if (arr[i + 1] < arr[i]) {
            return false;
        }
    }
    return true;
}

let arr1 = generateArr(300000, -1000, 1000);
let arr2 = [...arr1];
let arr3 = [...arr1];


console.log(arr1, 'origin');

console.time('pivot = median of three');
quickSort(arr1, getPivotIndexM);
console.timeEnd('pivot = median of three');
console.log('sorted', check(arr1));

console.log('   - - -');

console.time('pivot = right');
quickSort(arr2, getPivotIndexH);
console.timeEnd('pivot = right');
console.log('sorted', check(arr2));

console.log('   - - -');

console.time('pivot = random');
quickSort(arr3, getPivotIndexR);
console.timeEnd('pivot = random');
console.log('sorted', check(arr3));