class MaxHeap {
    constructor(arr = []) {
        this.arr = arr;
        if (this.arr.length){
            for (let i = Math.floor(arr.length/2) -1; i >= 0; i--) {
                this.sinkDown(i);
            }
        }
    }

    swap(a, b) {
        let t = this.arr[a];
        this.arr[a] = this.arr[b];
        this.arr[b] = t;
    }

    insert(value) {
        this.arr.push(value);
        this.bubbleUp();
    }

    bubbleUp(index = this.arr.length - 1) {
        while (index > 0) {
            let parentIndex = Math.floor((index - 1) / 2);
            if (this.arr[parentIndex] >= this.arr[index]) {
                break;
            }

            this.swap(index, parentIndex);
            index = parentIndex;
        }
    }

    pop() {
        this.swap(0, this.arr.length - 1);
        let result = this.arr.pop();
        this.sinkDown();

        return result;
    }

    sinkDown(index = 0) {
        while (index < this.arr.length - 1) {
            let maxChildIndex = index * 2 + 1;
            if (this.arr[maxChildIndex + 1] > this.arr[maxChildIndex]) {
                maxChildIndex++;
            }
            if (this.arr[maxChildIndex] > this.arr[index]) {
                this.swap(index, maxChildIndex);
                index = maxChildIndex;
            } else {
                break;
            }

        }
    }

    top() {
        return this.arr[0];
    }
}

function heapSort(arr) {
    function swap(a, b) {
        let t = arr[a];
        arr[a] = arr[b];
        arr[b] = t;
    }
    function sinkDown(index, len = arr.length) {
        while (index < len - 1) {
            let maxChildIndex = index * 2 + 1;
            if ((maxChildIndex + 1 < len)&&(arr[maxChildIndex + 1] > arr[maxChildIndex])) {
                maxChildIndex++;
            }
            if ((maxChildIndex < len) && (arr[maxChildIndex] > arr[index])) {
                swap(index, maxChildIndex);
                index = maxChildIndex;
            } else {
                break;
            }
        }
    }
    for (let i = Math.floor(arr.length/2) -1; i >= 0; i--) {
        sinkDown(i);
    }

    let len = arr.length;
    while (len) {
        // debugger;
        swap(0, len - 1);
        len--;
        sinkDown(0, len);
    }
}

//----------------------- проверка для MaxHeap -----------------------
// let tempArr = [17, 6, 9, 22, -7, 25, 0, 53, 8, 1.5, -7];
// let newHeap = new MaxHeap();
//
// for (let i = 0; i < tempArr.length; i++) {
//     newHeap.insert(tempArr[i]);
//     // console.log(newHeap.arr);
//
//     // if ((i + 1) % 3 === 0) {
//     //     console.log(newHeap.pop());
//     //     console.log(newHeap.arr);
//     // }
// }
// console.log(newHeap.arr);
//
// let heap2 = new MaxHeap(tempArr);
// console.log(heap2.arr);

//-------------------------------------------------------------------

//----------------------- проверка для heapSort -----------------------
function generateArr(n, min, max) {
    let arr = [];
    for (let i = 0; i < n; i++) {
        arr[i] = Math.floor(Math.random() * (max - min + 1)) + min;
    }
    return arr;
}
function check(arr) {
    for (let i = 0; i < arr.length - 1; i++) {
        if (arr[i + 1] < arr[i]) {
            return false;
        }
    }
    return true;
}

let arr1 = generateArr(1000, -1000, 1000);
heapSort(arr1);
console.log(arr1);
console.log(check(arr1));