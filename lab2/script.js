function selectionSort(array) {

     for (let checked = 0; checked < array.length; checked++) {
        let minIndex = checked;
        for (let i = checked + 1; i < array.length; i++) {
            if (array[i] < array[minIndex]) {
                minIndex = i;
            }
        }
        if (checked !== minIndex) {
            let x = array[checked];
            array[checked] = array[minIndex];
            array[minIndex] = x;
        }
    }
    return array;
}

    function insertionSort(array) {

        for (let i = 1; i < array.length; i++){
            for (let j = i; j > 0; j--){
                if (array[j] < array [j - 1]) {
                    let x = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = x;
                } else {
                    break;
                }
            }
        }
        return array;
    }

let arr = [];
for (let i = 0; i < 25000; i++) {
    arr[i] = Math.floor(Math.random() * 10000);
}

console.log(arr);
let temp = [...arr];
console.time('selectSort');
const result1 = selectionSort(temp);
console.timeEnd('selectSort');

temp = [...arr];
console.time('insertionSort');
const result2 = insertionSort(temp);
console.timeEnd('insertionSort');

temp = [...arr];
console.time('native');
const result3 = temp.sort((a, b) => a - b);
console.timeEnd('native');

console.log(result1);
console.log(result2);
console.log(result3);