class LLNode {
    constructor(key, value, next = null) {
        this.key = key;
        this.value = value;
        this.next = next;
    }
}

class HashTable {
    constructor() {
        this.arr = new Array(31);
        this.keyCount = 0;
        this.lowerLimit = 0.25;
        this.upperLimit = 1.25;
    }

    getLoadFactor() {
        return this.keyCount / this.arr.length;
    }

    getIndex(key, len = this.arr.length) {
        let hash = this.hashCode(key);
        return hash % len;
    }

    hashCode(key) {
        if (Number.isFinite(key)) {
            key = key.toString();
        }
        if (typeof key === 'string') {
            let hash = 0xdeadbeef;
            for (let i = 0; i < key.length; i++)
                hash = Math.imul(hash ^ key.charCodeAt(i), 2654435761);
            return (hash ^ hash >>> 16) >>> 0;
        } else {
            throw new Error('Unsupported key type');
        }
    }

    set(key, value) {
        let index = this.getIndex(key);
        let head = this.arr[index];
        if (!head) {
            head = new LLNode();
            this.arr[index] = head;
        }
        let current = head.next;
        while (current) {
            if (current.key === key) {
                current.value = value;
                return;
            }
            current = current.next;
        }
        head.next = new LLNode(key, value, head.next);
        this.keyCount++;
        if (this.getLoadFactor() > this.upperLimit) {
            let newSize = this.getNextPrimeNumber(this.arr.length * 2);
            this.resize(newSize);
        }
    }

    get(key) {
        let index = this.getIndex(key);
        let current = this.arr[index];
        while (current) {
            if (current.key === key) {
                return current.value;
            }
            current = current.next;
        }
    }

    delete(key) {
        let index = this.getIndex(key);
        let head = this.arr[index];
        if (!head) {
            return;
        }
        let current = head.next;
        let prev = head;
        while (current) {
            if (current.key === key) {
                prev.next = current.next;
                this.keyCount--;
                if (this.getLoadFactor() < this.lowerLimit && this.arr.length > 31) {
                    let newSize = this.getPrevPrimeNumber(Math.floor(this.arr.length / 2));
                    this.resize(newSize);
                }
                return;
            }
            prev = current;
            current = current.next;
        }
    }

    getPrevPrimeNumber(n) {
        for (let i = n - 1; i >= 31; i--) {
            if (this.isPrime(i)) return i;
        }
        return 31;
    }

    getNextPrimeNumber(n) {
        // Bertrand's theorem - there always exists at least one prime number p with n < p < 2n − 2 for n > 3.
        for (let i = n + 1; i < 2 * n; i++) {
            if (this.isPrime(i)) return i;
        }
    }

    isPrime(n) {
        if (n % 2 === 0 || n % 3 === 0) return false;

        for (let i = 5; i * i <= n; i = i + 6) {
            if (n % i === 0 || n % (i + 2) === 0) return false;
        }

        return true;
    }

    resize(newSize) {
        this.keyCount = 0;
        let oldArr = this.arr;
        this.arr = new Array(newSize);
        for (let i = 0; i < oldArr.length; i++) {
            if (oldArr[i] === undefined) {
                continue;
            }
            let current = oldArr[i].next;
            while (current) {
                this.set(current.key, current.value);
                current = current.next;
            }
        }
    }
}

let newHT = new HashTable();
const aCharCode = 'a'.charCodeAt(0);

function getRandInt(max, min) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function genKey() {
    const len = getRandInt(20, 5);
    const ans = [];
    for (let i = 0; i < len; i++) {
        ans.push(String.fromCharCode(getRandInt(0, 25) + aCharCode));
    }
    return ans.join('');
}

const map = new Map();
for (let i = 0; i < 100; i++) {
    newHT.set(i, i + 1000);
    map.set(i, i + 1000);
}

for (let i = 2; i < 100; i++) {
    newHT.delete(i);
    map.delete(i);
}

// for (let i = 0; i < 100; i++){
//     console.log(newHT.get(i));
// }

const keys = new Set();
while (keys.size < 10000) {
    keys.add(genKey());
}

let i = 0;
for (const key of keys) {
    newHT.set(key, i);
    map.set(key, i);
    i++;
}

for (const key of map.values()) {
    if (newHT.get(key) !== map.get(key)) {
        console.log(key, newHT.get(key), map.get(key))
    }
}
console.log(newHT.keyCount === map.size);

console.log(newHT);