function quickSortM(arr, left = 0, right = arr.length - 1) {

    if (left < right) {
        let p = partitionM(arr, left, right); // p = pivot index

        quickSortM(arr, left, p - 1);
        quickSortM(arr, p + 1, right);
    }
}
function partitionM(arr, left, right) {
    let mid = Math.floor((left + right) / 2);

    // выбор опорной точки (pivot) по медиане трех
    if (arr[left] > arr[mid] ){
        let tmp = arr[left];
        arr[left] = arr[mid];
        arr[mid] = tmp;
    }
    if (arr[right] > arr[mid]) {
        let tmp = arr[right];
        arr[right] = arr[mid];
        arr[mid] = tmp;
    }
    if (arr[left] > arr[right]) {
        let tmp = arr[left];
        arr[left] = arr[right];
        arr[right] = tmp;
    }
    // console.log(`       медиана трех `, arr, `l(${left}):`, arr[left], `m(${mid}):`, arr[mid], `r(${right}):`, arr[right]);

    let pivot = arr[right];
    let i = left - 1; // Index of smaller element

    for (let j = left; j <= right - 1; j++) {
        // If current element is smaller than the pivot
        if (arr[j] < pivot) {
            i++;
            let tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }
    }

    let tmp = arr[i + 1];
    arr[i + 1] = arr[right];
    arr[right] = tmp;

    // console.log(arr, 'left:', left, 'right:', right, `p[${(i+1)}]=`, arr[i+1]);

    return (i + 1);
}

function quickSortH(arr, low = 0, high = arr.length - 1) {

    if (low < high) {
        let p = partitionH(arr, low, high); // p = pivot index

        quickSortH(arr, low, p - 1);
        quickSortH(arr, p + 1, high);
    }
}
function partitionH (arr, low, high) {
    // выбор опорной точки (pivot) через последний элемент
    let pivot = arr[high];
    let i = low - 1;

    for (let j = low; j <= high - 1; j++) {

        if (arr[j] < pivot) {
            i++;
            let tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }
    }

    let tmp = arr[i + 1];
    arr[i + 1] = arr[high];
    arr[high] = tmp;

    // console.log(arr, 'p[' + (i+1) + ']=', arr[i+1], ' low:', low, 'high:', high);

    return (i + 1);
}

function quickSortR(arr, left = 0, right = arr.length - 1) {
    // debugger;
    if (left < right) {
        let p = partitionR(arr, left, right); // p = pivot index

        quickSortR(arr, left, p - 1);
        quickSortR(arr, p + 1, right);
    }
}
function partitionR(arr, left, right) {
    // выбор опорной точки (pivot) рандомно
    let rndm = Math.floor(Math.random() * (right - left + 1)) + left;

    if (rndm !== right) {
        let tmp = arr[rndm];
        arr[rndm] = arr[right];
        arr[right] = tmp;
    }

    let pivot = arr[right];

    let i = left - 1;

    for (let j = left; j <= right - 1; j++) {

        if (arr[j] < pivot) {
            i++;
            let tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }
    }

    let tmp = arr[i + 1];
    arr[i + 1] = arr[right];
    arr[right] = tmp;

    // console.log(arr, 'left:', left, 'right:', right, `p[${(i+1)}]=`, arr[i+1]);

    return (i + 1);
}

function generateArr(n, min, max) {
    let arr = [];
    for (let i = 0; i < n; i++) {
        arr[i] = Math.floor(Math.random() * (max - min + 1)) + min;
    }
    return arr;
}

function check(arr) {
    for (let i = 0; i < arr.length - 1; i++) {
        if (arr[i + 1] < arr[i]) {
            return false;
        }
    }
    return true;
}

let arr1 = generateArr(9000, -1000, 1000);
let arr2 = [...arr1];
let arr3 = [...arr1];
// let arr = [-10, -5, 3, -7, 4, 9];
// let arr =  [-4, 8, -8, 0, 6, -5];

console.log(arr1, 'origin');

console.time('pivot = median of three');
quickSortM(arr1);
console.timeEnd('pivot = median of three');
console.log('sorted', check(arr1));

console.log('   - - -');

console.time('pivot = high');
quickSortH(arr2);
console.timeEnd('pivot = high');
console.log('sorted', check(arr2));

console.log('   - - -');

console.time('pivot = random');
quickSortR(arr3);
console.timeEnd('pivot = random');
console.log('sorted', check(arr3));