class LowArray {
    constructor(size) {
        this.array = new Array(size);
        this.filled = 0;
    }

    insert(value) {
        if (this.filled === this.array.length) {
            return false;
        }
        this.array[this.filled] = value;
        this.filled++;
        return true;
    }

    findIndex(value) {
        for (let i = 0; i < this.filled; i++) {
            if (this.array[i] === value) {
                return i;
            }
        }
        return -1;
    }
    
    find(value) {
        return this.findIndex(value) >= 0;
    }
    
    delete(value) {
        let itemIndex = this.findIndex(value);

        if (itemIndex === -1) {
            return false;
        }
        for (let i = itemIndex; i < this.filled - 1; i++) {
            this.array[i] = this.array[i + 1];
        }
        this.filled--;
        this.array[this.filled] = undefined;
        return true;
    }

    selectionSort() {
        for (let checked = 0; checked < this.filled; checked++) {
            let minIndex = checked;
            for (let i = checked + 1; i < this.filled; i++) {
                if (this.array[i] < this.array[minIndex]) {
                    minIndex = i;
                }
            }
            if (checked !== minIndex) {
                let x = this.array[checked];
                this.array[checked] = this.array[minIndex];
                this.array[minIndex] = x;
            }
        }
        return this.array;
    }

    insertionSort() {
        for (let i = 1; i < this.filled; i++){
            for (let j = i; j > 0; j--){
                if (this.array[j] < this.array [j - 1]) {
                    let x = this.array[j];
                    this.array[j] = this.array[j - 1];
                    this.array[j - 1] = x;
                } else {
                    break;
                }
            }
        }
        return this.array;
    }
}

const la = new LowArray(10);

for (let i = 0; i < 10; i++) {
    la.insert(i);
    console.log('insert', i, [...la.array]);
}


console.log('find', 5, la.find(5));   
console.log('find', 1000, la.find(1000)); 

console.log('delete', la.delete(3));  
console.log([...la.array]);
console.log('delete', la.delete(3));  
console.log([...la.array]);

la.insert(15);
console.log('insert', 15, [...la.array]);
console.log(la.insert(16));
console.log('insert', 16, [...la.array]);

const la2 = new LowArray(2500);
for (let i = 0; i < 2500; i++) {
    la2.insert(Math.floor(Math.random() * 100));
}

console.log('before', [...la2.array]);
console.time('selection sort');
la2.selectionSort();
console.timeEnd('selection sort');
console.log('after selection sort', la2.array);

const la3 = new LowArray(2500);
for (let i = 0; i < 2500; i++) {
    la3.insert(Math.floor(Math.random() * 100));
}
console.log('before', [...la3.array]);
console.time('insertion sort');
la3.insertionSort();
console.timeEnd('insertion sort');
console.log('after insertion sort', la3.array);