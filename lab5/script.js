// mergeSort через стеки

class Stack {
    constructor(maxSize) {
        this.array = new Array(maxSize);
        this.size = 0;
    }

    push(value) {
        if (this.isFull()) {
            return false;
        }
        this.array[this.size] = value;
        this.size++;
        return true;
    }

    pop() {
        if (this.isEmpty()) {
            return undefined;
        }
        let deleted = this.array[this.size - 1];
        this.array[this.size - 1] = undefined;
        this.size--;

        return deleted;
    }

    peek() {
        return this.array[this.size - 1];
    }

    isFull() {
        return this.size >= this.array.length;
    }

    isEmpty() {
        return this.size === 0;
    }
}

function sort(arr) {
    let stack1 = new Stack(arr.length);
    let stack2 = new Stack(arr.length);

    for (let i of arr) {
        stack1.push([i]);
    }
    while (stack1.size > 1) {
        while (stack1.size > 1) {
            let el1 = stack1.pop();
            let el2 = stack1.pop();

            stack2.push(merge(el1, el2));
        }
        while (stack1.size > 0) {
            stack2.push(stack1.pop());
        }
        let temp = stack1;
        stack1 = stack2;
        stack2 = temp;
    }

    let sorted = stack1.pop();

    for (let i = 0; i < arr.length; i++) {
        arr[i] = sorted[i];
    }
}

function merge(el1, el2) {
    let result = [];

    let i = 0;
    let j = 0;

    while ( i < el1.length && j < el2.length) {
        if (el1[i] < el2[j]) {
            result.push(el1[i]);
            i++;
        } else {
            result.push(el2[j]);
            j++;
        }
    }

    while (i < el1.length ) {
        result.push(el1[i]);
        i++;
    }
    while (j < el2.length ) {
        result.push(el2[j]);
        j++;
    }

    console.log(el1, el2, result);
    return result;
}

let arr = [];
for (let i = 0; i < 10; i++) {
    arr[i] = Math.floor(Math.random() * 100);
}

console.log(arr);
sort(arr);
console.log(arr);