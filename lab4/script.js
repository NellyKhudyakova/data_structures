class Node {
    constructor(item, next = null, prev = null) {
        this.item = item;
        this.next = next;
        this.prev = prev;
    }
}

class List {
    constructor(){
        this.head = new Node();
        this.tail = new Node();
        this.head.next = this.tail;
        this.tail.prev = this.head;
    }

    find(item) {
        let current = this.head.next;
        while (current !== this.tail) {
            if (current.item === item) {
                return current;
            }
            current = current.next;
        }
        return undefined;
    }

    insertStart(item) {
        let node = new Node(item, this.head.next, this.head);
        this.head.next = node;
        node.next.prev = node;
    }

    insertEnd(item) {
        let node = new Node(item, this.tail, this.tail.prev);
        this.tail.prev = node;
        node.prev.next = node;
    }

    insertAfter(item, preItem) {
        // preItem - значение, после которого нужно вставить item
        let preNode = this.find(preItem);
        if (!preNode) {
            return false;
        }
        let newNode = new Node(item, preNode.next, preNode);
        preNode.next = newNode;
        newNode.next.prev = newNode;
        return true;
    }

    deleteStart() {
        if (this.head.next === this.tail) {
            return undefined;
        }
        let deleteItem = this.head.next.item;
        this.head.next = this.head.next.next;
        this.head.next.prev = this.head;

        return deleteItem;
    }

    deleteEnd() {
        if (this.head.next === this.tail) {
            return undefined;
        }
        let deleteItem = this.tail.prev.item;
        this.tail.prev = this.tail.prev.prev;
        this.tail.prev.next = this.tail;

        return deleteItem;
    }

    deleteItem(item) {
        let node = this.find(item);
        if (!node) {
            return false;
        }
        node.prev.next = node.next;
        node.next.prev = node.prev;

        return true;
    }

    toArr(nodeStart = this.head.next, nodeFinish = this.tail.prev) {
        let arr = [];
        let current = nodeStart;
        while (current !== nodeFinish.next) {
            arr.push(current.item);
            current = current.next;
        }
        return arr;
    }

    static strToList(string) {
        let list = new List();

        for (let char of string) {
            list.insertEnd(char);
        }

        return list;
    }
}
let romanToArab = new Map([
    ['I',1],
    ['V',5],
    ['X',10],
    ['L',50],
    ['C',100],
    ['D',500],
    ['M',1000]
]);

let canBeRepeated = new Set('IXCM');

function isValidRomanNum(list, nodeStart, nodeFinish) {
    let repeatCounter = 1;

    let current = nodeStart;

    while (current !== nodeFinish.next) {

        if (!romanToArab.has(current.item)) {
            return 'Введена несуществующая римская цифра';
        }

        const prevItem = current !== nodeStart ? current.prev.item : 0;
        if (current.item === prevItem) {
            repeatCounter++;

            if (repeatCounter > 3) {
                return 'Использовано более 3х одинаковых символов подряд';
            }

            if (!canBeRepeated.has(current.item)) {
                return 'Не могут повторяться цифры, которые не являются степенью 10';
            }
        } else {
            repeatCounter = 1;
        }

        let prevVal = romanToArab.get(current.prev.item) || 0;
        let curVal = romanToArab.get(current.item);
        if (prevVal && curVal > prevVal) {
            if (!canBeRepeated.has(current.prev.item)) {
                return 'Не могут вычитаться цифры, которые не являются степенью 10';
            }

            let nextVal = current !== nodeFinish ? romanToArab.get(current.next.item) || 0 : 0;
            if (nextVal >= prevVal) {
                return 'Следующая после текущей цифра не может быть меньше предшествующей текущей';
            }

            let prevprevVal = romanToArab.get(current.prev.prev.item);
            if (prevprevVal && !(prevprevVal > prevVal)) {
                return 'Предпредыдущая цифра не может быть больше предыдущей';
            }
        }
        current = current.next;
    }
    return true;
}

function convertRomanToArabic(list, nodeStart = list.head.next, nodeFinish = list.tail.prev) {
    if (typeof isValidRomanNum(list, nodeStart, nodeFinish) === 'string') {
        return isValidRomanNum(list, nodeStart, nodeFinish);
    }
    let result = 0;
    let current = nodeStart;
    while(current !== nodeFinish.next) {
        let curVal = romanToArab.get(current.item);
        let nextVal = current !== nodeFinish ? romanToArab.get(current.next.item) || 0 : 0;
        if (curVal < nextVal) {
            result -= curVal;
        } else {
            result += curVal;
        }
        current = current.next;
    }
    return result;
}

function check3(list) {
    let nodeStart = list.head.next;
    let nodeFinish = nodeStart;
    let calculations = [];
    for (let i = 0; i < 2; i++) {
        if (nodeFinish.next === list.tail) {
            break;
        }
        nodeFinish = nodeFinish.next;
    }
    do {
        console.log(list.toArr(nodeStart, nodeFinish), convertRomanToArabic(list,nodeStart,nodeFinish));
        calculations.push([list.toArr(nodeStart, nodeFinish), convertRomanToArabic(list,nodeStart,nodeFinish)]);
        nodeStart = nodeStart.next;
        nodeFinish = nodeFinish.next;
    } while (nodeFinish !== list.tail);
    console.log(list.toArr(), convertRomanToArabic(list));
    console.log('---');

    calculations.push([list.toArr(), convertRomanToArabic(list)]);
    calculations.push([['-'],['-']]);
    // let result = [list.toArr(), convertRomanToArabic(list)];
    return calculations;
}

function getFormData() {
    let data = document.forms["rNumForm"].elements["rNumField"].value;
    data = data.toUpperCase();
    let outPlace = document.getElementById('output');
    let Output = check3(List.strToList(data));

    for (let item of Output) {
        let newLi = document.createElement('li');
        outPlace.appendChild(newLi);

        let rnumA = item[0];
        let rnumS = '';
        for (let i of rnumA) {
            rnumS = rnumS + i;
        }
        newLi.innerText = rnumS + ' - ' + item[1];
    }
    // console.log(`data = ${data}`);
}

// check3(List.strToList('XXVI'));
// check3(List.strToList('CMC'));
// check3(List.strToList('IIII'));
// check3(List.strToList('VX'));
// check3(List.strToList('IVX'));
// check3(List.strToList('IIIV'));
// check3(List.strToList('CMXXC'));
// check3(List.strToList('VM'));
// check3(List.strToList('CMIX'));
// check3(List.strToList('CCCIV'));
// check3(List.strToList('CDLXXVII'));

// const intToRoman = [[1000, 'M'], [900, 'CM'], [500, 'D'], [400, 'CD'], [100, 'C'], [90, 'XC'], [50, 'L'], [40, 'XL'],
//     [10, 'X'], [9, 'IX'], [5, 'V'], [4, 'IV'], [1, 'I']];
// function arabicToRoman(int) {
//     let result = '';
//     for (const [num, roman] of intToRoman) {
//         while (int >= num) {
//             result += roman;
//             int -= num;
//         }
//     }
//     return result;
// }
//
// const numMap = new Map();
// for (let i = 1; i < 4000; i++) {
//     numMap.set(arabicToRoman(i), i);
// }
//
// for (const [roman, arabic] of numMap) {
//     if (convertRomanToArabic(List.strToList(roman)) !== arabic) {
//         console.log(roman);
//     }
// }
// console.log('finished');

