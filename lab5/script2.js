// mergeSort через рекурсию

function sort(arr, left = 0, right = arr.length - 1) {

    if (left < right) {
        let middle = Math.floor((left + right)/2);

        sort(arr, left, middle);
        sort(arr, middle+1, right);

        merge(arr, left, middle, right);
    }
}

function merge(arr, left, middle, right) {

    let lengthL = middle - left + 1;
    let lengthR = right - middle;

    let arrL = arr.slice(left, middle + 1);
    let arrR = arr.slice(middle + 1, right + 1);

    let i = 0;
    let j = 0;

    let k = left; // индекс в исходном массиве, с которого будут вставляться элементы
    while ( i < lengthL && j < lengthR) {
        if (arrL[i] < arrR[j]) {
            arr[k] = arrL[i];
            i++;
        } else {
            arr[k] = arrR[j];
            j++;
        }
        k++;
    }

    while (i < lengthL ) {
        arr[k] = arrL[i];
        i++;
        k++;
    }
    while (j < lengthR ) {
        arr[k] = arrR[j];
        j++;
        k++;
    }

    console.log(arr, arrL, arrR);
}

let arr = [1, 8, 3, 4, 22, -1];

console.log(arr);
sort(arr);
console.log(arr);