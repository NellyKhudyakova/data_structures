class Stack {
    constructor(maxSize) {
        this.array = new Array(maxSize);
        this.size = 0;
    }

    push(value) {
        if (this.isFull()) {
            return false;
        }
        this.array[this.size] = value;
        this.size++;
        return true;
    }

    pop() {
        if (this.isEmpty()) {
            return undefined;
        }
        let deleted = this.array[this.size - 1];
        this.array[this.size - 1] = undefined;
        this.size--;

        return deleted;
    }

    peek() {
        return this.array[this.size - 1];
    }

    isFull() {
        return this.size >= this.array.length;
    }

    isEmpty() {
        return this.size === 0;
    }
}

class Calculator {

    static strToArr(str) {
        let set = new Set('()-+*/');
        let arr = str.split(' ');

        for (let i = 0; i < arr.length; i++) {
            if (!set.has(arr[i])) {
                arr[i] = Number.parseInt(arr[i]);
            }
        }
        return arr;
    }

    static arrToRPN(arr) {
        let result = [];
        let opStack = new Stack(1000);
        let precedence = { //приоритет
            '(': 0,
            '-': 1,
            '+': 1,
            '*': 2,
            '/': 2,
        };

        for (let item of arr) {
            if (typeof item === 'number') {
                result.push(item);
            } else if (item === '(') {
                opStack.push(item);
            } else if (item === ')') {
                while (opStack.peek() !== '(') {
                    result.push(opStack.pop());
                }
                opStack.pop();
            } else {
                let itemPrecedence = precedence[item];
                let lastOpPrecedence = opStack.isEmpty() ? 0 : precedence[opStack.peek()];
                if (itemPrecedence < lastOpPrecedence) {
                    while (!opStack.isEmpty() && opStack.peek() !== '(') {
                        result.push(opStack.pop());
                    }
                    opStack.push(item);
                } else {
                    opStack.push(item);
                }
            }
        }
        while (!opStack.isEmpty()) {
            result.push(opStack.pop());
        }
        return result;
    }

    static solveRPN(RPN) {
        let stack = new Stack(1000);
        for (let item of RPN) {
            if (typeof item === 'number') {
                stack.push(item);
            } else {
                let num2 = stack.pop();
                let num1 = stack.pop();
                switch (item) {
                    case '*':
                        stack.push(num1 * num2);
                        break;
                    case '+':
                        stack.push(num1 + num2);
                        break;
                    case '-':
                        stack.push(num1 - num2);
                        break;
                    case '/':
                        stack.push(num1 / num2);
                        break;
                }
            }
        }
        return stack.pop();
    }

    static solve(str) {
        let arr = Calculator.strToArr(str);
        let rpn = Calculator.arrToRPN(arr);
        let answer = Calculator.solveRPN(rpn);
        console.log(arr);
        console.log(rpn);
        return answer;
    }
}

function calcFormData() {
    let data = document.forms["NumForm"].elements["NumField"].value;
    // data = data;
    let outPlace = document.getElementById('output');
    // let Output = Calculator.solve(data);
    // outPlace.innerText = Output;

    outPlace.innerText = Calculator.solve(data);
    console.log(Calculator.solve(data));
}

// console.log(Calculator.solve('2 + ( -30 - 6 ) / 2 + 3 * 4'));
// console.log(Calculator.solve('( 1 + 2 ) * 3 / 2'));
