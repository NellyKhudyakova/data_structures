class TNode {
    constructor(item) {
        this.item = item;
        this.left = null;
        this.right = null;
    }
    
}

class Tree {
    constructor() {
        this.root = null;
    }

    insert(value) {
        let node = new TNode(value);
        if(!this.root) {
            this.root = node;
        } else {
            let current = this.root;
            let parent;
            while (current) {
                parent = current;
                if (value < current.item) {
                    current = current.left;
                } else {
                    current = current.right;
                }
            }
            if (value < parent.item) {
                parent.left = node;
            } else {
                parent.right = node;
            }
        }
    }

    // value - значение, которое ищем, recordPath - запись пути обхода
    // startNode - нода, с которой начинаем обход
    find_(value, recordPath, startNode = this.root) {
        let current = startNode;
        let path = recordPath ? [] : undefined;
        let parent = null;
        while (current) {
            if (recordPath) {
                path.push(current.item);
            }
            if (value === current.item) {
                return [current, parent, path];
            } else {
                parent = current;
                if (value < current.item) {
                    current = current.left;
                } else {
                    current = current.right;
                }
            }
        }
        return [undefined, undefined, path];
    }

    find (value) {
        let [current, , path] = this.find_(value, true);
        return [current, path];
    }

    findBFS(value) {
        let path = [];
        if (!this.root) {
            return [false, path];
        }
        let queue = [this.root];

        while (queue.length) {
            let current = queue.shift();
            path.push(current.item);
            if (value === current.item) {
                return [true, path];
            }
            if (current.left) {
                queue.push(current.left);
            }
            if (current.right) {
                queue.push(current.right);
            }
        }
        return [false, path];
    }

    delete(value, startNode = this.root) {
        let [current, parent] = this.find_(value, false, startNode);
        if (!current) {
            return false;
        }

        // если у удаляемой ноды нет потомков (она leaf)
        if (!current.left && !current.right) {
            if (!parent) { // и нет родителя => она root
                this.root = null;
            } else if (parent.left === current) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        }

        // если у удаляемой ноды только один потомок
        if ((current.left && !current.right)||(!current.left && current.right)) {
            let replacementNode = current.left || current.right; // нода НА которую мы заменяем текущую
            if (!parent) {
                this.root = replacementNode;
            } else if (parent.left === current) {
                parent.left = replacementNode;
            } else {
                parent.right = replacementNode;
            }
        }

        // если у удаляемой ноды оба 2 потомка
        if (current.left && current.right){
            let min = current.right;

            while (min.left) {
                min = min.left;
            }
            let minVal = min.item;
            current.item = -Infinity;
            this.delete(minVal, current);
            current.item = minVal;
        }
    }
}

let newTree = new Tree();
newTree.insert(2);
newTree.insert(1);
newTree.insert(5);
newTree.insert(3);
newTree.insert(7);
newTree.insert(6);
newTree.insert(8);
// newTree.insert(6);
// newTree.insert(8);
console.log(newTree);
// console.log(newTree.find_(7));
console.log(newTree.find(7));
console.log(newTree.findBFS(7));
newTree.delete(5);

